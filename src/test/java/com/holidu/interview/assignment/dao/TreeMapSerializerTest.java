package com.holidu.interview.assignment.dao;


import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.StringWriter;

@RunWith(SpringRunner.class)
public class TreeMapSerializerTest {

    private TreeMapSerializer ser;
    private TreeSet treeSet;

    @Before
    public void setup() {
        ser = new TreeMapSerializer();

        treeSet = new TreeSet();
        treeSet.getTreesByType().put("Test1", 5);
        treeSet.getTreesByType().put("Test2", 3);
        treeSet.getTreesByType().put("Test3", 15);
        treeSet.getTreesByType().put("Test4", 37);
    }

    @Test
    public void testSerialization() throws IOException, JSONException {
        JsonFactory factory = new JsonFactory();
        StringWriter jsonObjectWriter = new StringWriter();
        JsonGenerator generator = factory.createGenerator(jsonObjectWriter);

        ser.serialize(treeSet, generator, null);
        generator.close();

        JSONObject jsonObject = new JSONObject(jsonObjectWriter.toString());

        Assert.assertEquals(5, jsonObject.get("Test1"));
        Assert.assertEquals(3, jsonObject.get("Test2"));
        Assert.assertEquals(15, jsonObject.get("Test3"));
        Assert.assertEquals(37, jsonObject.get("Test4"));
    }
}
