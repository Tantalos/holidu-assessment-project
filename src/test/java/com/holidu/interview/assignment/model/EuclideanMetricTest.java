package com.holidu.interview.assignment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {EuclideanMetric.class})
public class EuclideanMetricTest {

    @Autowired
    @Qualifier("euclideanMetric")
    private Metric metric;

    private final Point center = new Point(0, 0);
    private static final double RADIUS = 20d;

    private static final double DELTA = 0.0005d;

    @Before
    public void setup() {
    }



    @Test
    public void testDistance() {
        Point p = new Point(50, 3);

        // symmetry
        Assert.assertEquals(metric.distance(p.x, p.y, center.x, center.y), metric.distance(center.x, center.y, p.x, p.y), DELTA);
    }

    private double distanceToCenter(Point p) {
        return metric.distance(p.x, p.y, center.x, center.y);
    }

    @Test
    public void testInside() {
        Point p = new Point(10.7, 16.7);
        Point p2 = new Point(-5.0, 19.1);
        Point p3 = new Point(-19.2, -4.4);
        Point p4 = new Point(19.5, -3.9);

        Assert.assertTrue(distanceToCenter(p) <= RADIUS);
        Assert.assertTrue(distanceToCenter(p2) <= RADIUS);
        Assert.assertTrue(distanceToCenter(p3) <= RADIUS);
        Assert.assertTrue(distanceToCenter(p4) <= RADIUS);
    }

    @Test
    public void testOutside() {
        Point p = new Point(10.8, 17.3);
        Point p2 = new Point(-5.7, 19.2);
        Point p3 = new Point(-19.5, -4.5);
        Point p4 = new Point(19.6, -4d);

        Assert.assertFalse(distanceToCenter(p) <= RADIUS);
        Assert.assertFalse(distanceToCenter(p2) <= RADIUS);
        Assert.assertFalse(distanceToCenter(p3) <= RADIUS);
        Assert.assertFalse(distanceToCenter(p4) <= RADIUS);

    }



    @Data
    @AllArgsConstructor
    class Point {
        private double x;
        private double y;
    }
}
