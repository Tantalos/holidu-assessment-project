package com.holidu.interview.assignment.service;


import com.holidu.interview.assignment.dao.Tree;
import com.holidu.interview.assignment.dao.TreeSet;
import com.holidu.interview.assignment.nycdata.NYCPlantInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

@Service
public class NYCPlantService {


    @Autowired
    public NYCPlantInterface nycAPI;

    public TreeSet getSurroundingTrees(double coordX, double coordY, double radius) {
        assert radius >= 0;
        radius = meterstToGeographicCoords(radius);
        List<Tree> trees = nycAPI.getPlantsWithinCircle(coordX, coordY, radius);

        if (trees == null) {
            // Could be a service failed exception
            throw new RuntimeException("Retrieval of trees failed");
        }

        TreeSet set = new TreeSet();
        Map<String, Integer> treeMap = set.getTreesByType();
        for (Tree tree : trees) {
            String treeSpCommonName = tree.getSpc_common();
            if (treeSpCommonName == null) {
                treeSpCommonName = "unknown";
            }
            if (!treeMap.containsKey(treeSpCommonName)) {
                treeMap.put(treeSpCommonName, 0);
            }
            treeMap.put(treeSpCommonName, treeMap.get(treeSpCommonName) + 1);
        }

        return set;
    }

    private static double meterstToGeographicCoords(double x) {
        return x / 111111d;
    }
}
