package com.holidu.interview.assignment.nycdata;

import com.holidu.interview.assignment.dao.Tree;

import java.util.List;

public interface NYCPlantInterface {

    /**
     * Returns all trees that are within a square (oriented to north) with a given side length r and the center at a
     * given point (x, y)
     *
     * @param x x coordinate of the center for the square
     * @param y y coordinate of the center for the square
     * @param r side legth of the square
     * @return a list of trees within the given square or null if an error occurred
     */
    List<Tree> getPlantsInArea(double x, double y, double r);

    /**
     * Returns all trees that are within a circle with a given radius r and the center at a
     * given point (x, y)
     *
     * @param x x coordinate of the center for the circle
     * @param y y coordinate of the center for the circle
     * @param r radius of the circle
     * @return a list of trees within the given square or null if an error occurred
     */
    List<Tree> getPlantsWithinCircle(double x, double y, double r);
}
