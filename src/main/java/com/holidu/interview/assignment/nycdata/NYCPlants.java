package com.holidu.interview.assignment.nycdata;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.holidu.interview.assignment.dao.Tree;
import com.holidu.interview.assignment.model.Metric;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
@Slf4j
public class NYCPlants implements NYCPlantInterface {

    @Value("${data.cityofnewyork.us.endpoint}")
    private String endpoint;

    @Autowired
    public Metric metric;

    private static final int LIMIT = 1000;

    public NYCPlants() throws IOException {
    }

    @Override
    public List<Tree> getPlantsInArea(double x, double y, double r) {
        assert r >= 0;
        double ol = x - r; // outerLeft
        double or = x + r; // outerRight
        double ou = y + r; // outerUpper
        double olw = y - r; // outerLower

        log.debug("Retrieve all trees from ({},{}) to ({},{})", ol, olw, or, ou);

        // Would be a much more efficient api call. This is currently not feasible with the given dataset and api
        // Note: within_circle() takes as first argument only location or point data types.
        // TODO query ="$select=spc_common, count(*) & $where=within_circle( make_point(latitude, longitude), ?, ?, ?) & $group=spc_common";
        String query = String.format( "$select=tree_id, spc_common, longitude, latitude&$where=latitude between %s and %s AND longitude between %s and %s", ol, or, olw, ou );
        query = query.replace(" ", "%20");
        String request = endpoint + "?" + query;

        try {
            log.debug("Fetching api resource: {}", request);
            List<Tree> trees = new ArrayList<>();
            List<Tree> fetchedTrees;
            int rounds = 0;
            do {
                String result = fetchRecource(request + "&$offset=" + rounds * LIMIT);
                fetchedTrees = mapTrees(result);
                trees.addAll(fetchedTrees);
                rounds++;
            } while (fetchedTrees.size() >= LIMIT);
            log.info("Fetched {} resources in {} rounds", trees.size(), rounds);
            return trees;
        } catch (JsonProcessingException e) {
            log.error("Error processing results of api", e);
        } catch (IOException e) {
            log.error("Error while connection with api", e);
        }
        return null;
    }

    @Override
    public List<Tree> getPlantsWithinCircle(double x, double y, double r) {
        List<Tree> trees = getPlantsInArea(x, y, r);

        // =====================================================
        // Additional filtering because of inefficient API usage
        // =====================================================
        Stream<Tree> stream = trees.stream().filter(t -> metric.distance(x, y, t.getLatitude(), t.getLongitude()) < r);
        List<Tree> filteredTrees = stream.collect(Collectors.toList());
        log.debug("Return {}/{} trees. {} trees dropped", filteredTrees.size(), trees.size(), trees.size() - filteredTrees.size());

        return filteredTrees;
    }

    private List<Tree> mapTrees(String jsonString) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Tree[] trees = mapper.readValue(jsonString, Tree[].class);
        return Arrays.asList(trees);
    }

    private String fetchRecource(String request) throws IOException {
        log.debug("Fetching api resource: {}", request);
        URL nycplants = new URL(request);

        URLConnection connection = nycplants.openConnection();
        HttpURLConnection httpConnection = (HttpURLConnection) connection;

        BufferedReader in = new BufferedReader(
                new InputStreamReader(httpConnection.getInputStream()));
        int status = httpConnection.getResponseCode();
        if (HttpStatus.valueOf(status) != HttpStatus.OK) {
            throw new IOException("Received wrong status code " + status);
        }

        String jsonResource = "";
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            jsonResource += inputLine;
        }
        in.close();

        return jsonResource;
    }
}
