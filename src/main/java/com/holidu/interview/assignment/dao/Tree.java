package com.holidu.interview.assignment.dao;

import lombok.Data;

@Data
public class Tree {
    private long tree_id;
    private String spc_common;

    private double longitude;
    private double latitude;
}
