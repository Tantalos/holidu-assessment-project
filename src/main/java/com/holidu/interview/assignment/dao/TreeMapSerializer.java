package com.holidu.interview.assignment.dao;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Map.Entry;

public class TreeMapSerializer extends JsonSerializer<TreeSet> {

    @Override
    public void serialize(TreeSet treeSet, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        for (Entry<String, Integer> entry : treeSet.getTreesByType().entrySet()) {
            jsonGenerator.writeNumberField(entry.getKey(), entry.getValue());
        }
        jsonGenerator.writeEndObject();
    }
}
