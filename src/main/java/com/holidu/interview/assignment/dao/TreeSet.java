package com.holidu.interview.assignment.dao;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;


import java.util.HashMap;
import java.util.Map;

@Data
@JsonSerialize(using = TreeMapSerializer.class)
public class TreeSet {
    private Map<String, Integer> treesByType = new HashMap<>();
}
