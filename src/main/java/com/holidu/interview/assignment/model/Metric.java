package com.holidu.interview.assignment.model;

public interface Metric {
    double distance(double firstX, double firstY, double secondX, double secondY);
}
