package com.holidu.interview.assignment.model;

import org.springframework.stereotype.Component;

@Component
public class EuclideanMetric implements Metric {

    @Override
    public double distance(double firstX, double firstY, double secondX, double secondY) {
        return Math.sqrt(Math.pow(firstX - secondX, 2) + Math.pow(firstY - secondY, 2));
    }
}
