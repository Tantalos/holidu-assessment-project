package com.holidu.interview.assignment.controller;

import com.holidu.interview.assignment.dao.TreeSet;
import com.holidu.interview.assignment.service.NYCPlantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PlantSearchEndpoint {

    @Autowired
    NYCPlantService nycplantService;


    @RequestMapping(
            name = "plants-in-area",
            method = RequestMethod.GET,
            value = "/plants/in-area"
    )
    @ResponseBody
    public ResponseEntity getSurroundingTrees(@RequestParam double x, @RequestParam double y, @RequestParam double r) {
        if(r < 0) {
            return ResponseEntity.badRequest().body("The argument r cannot be negative");
        }

        TreeSet trees;
        try {
            trees = nycplantService.getSurroundingTrees(x, y, r);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("An internal server error occurred. Please try again later.");
        }

        return ResponseEntity.ok(trees);
    }
}
